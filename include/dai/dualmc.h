/*  This file is part of libDAI - http://www.libdai.org/
 *
 *  Copyright (c) 2006-2011, The libDAI authors. All rights reserved.
 *
 *  Use of this source code is governed by a BSD-style license that can be found in the LICENSE file.
 */


/// \file
/// \brief Defines class DUALMC, which implements Importance Sampling in the Dual Forney Graph
/// See "Efficient Monte Carlo Methods for the Potts Model at Low Temperature (M. Molkaraie, V. Gómez) http://arxiv.org/abs/1506.07044


#ifndef __defined_libdai_dualmc_h
#define __defined_libdai_dualmc_h


#include <dai/dai_config.h>
#ifdef DAI_WITH_DUALMC


#include <string>
#include <dai/daialg.h>
#include <dai/factorgraph.h>
#include <dai/properties.h>
#include <dai/enum.h>

#include <gsl/gsl_rng.h>
#include <map>

namespace dai {

using namespace std;
using namespace boost;

typedef adjacency_list < vecS, vecS, undirectedS, no_property, property < edge_weight_t, Real > > BoostGraph;
typedef graph_traits < BoostGraph >::edge_descriptor BoostEdge;
typedef graph_traits < BoostGraph >::vertex_descriptor BoostVertex;
typedef std::pair<int, int> E;

class Stats {
    public:
        Stats() : m_n(0) {}
        void clear() { m_n = 0; }
        void push(double x) {
            m_n++;
            // See Knuth TAOCP vol 2, 3rd edition, page 232
            if (m_n == 1) {
                m_oldM = m_newM = x;
                m_oldS = 0.0;
            } else {
                m_newM = m_oldM + (x - m_oldM)/m_n;
                m_newS = m_oldS + (x - m_oldM)*(x - m_newM);
    
                // set up for next iteration
                m_oldM = m_newM; 
                m_oldS = m_newS;
            }
        }
        int numDataValues() const { return m_n; }
        double mean() const { return (m_n > 0) ? m_newM : 0.0; }
        double variance() const { return ( (m_n > 1) ? m_newS/(m_n - 1) : 0.0 ); }
        double stdev() const { return sqrt( variance() ); }

    private:
        int m_n;
        double m_oldM, m_newM, m_oldS, m_newS;
};


/// This class implements the solution of a linear system of equations
/** Explain algorithm 
 *  here 
 */
class LinSysMod {
	protected:
		size_t q;						// state size
		size_t vs, vss,eqss;
		vector<size_t> order;			// vector with the Gaussian elimination ordering (size of the MST, region B)
		vector<vector<size_t> > vidx;	// vector of dependences for each variable  (size of the graph, not all entries are used)
		vector<vector<size_t> > vidx2;	// vector of dependences for each variable  (size of the graph, not all entries are used)
		vector<Real > Jk0;			// this is exp(Jk) + q - 1 for each k in region B
		vector<Real > Jk;				// this is exp(Jk) - 1 for each k in region B
		vector<char> invq;
		vector<size_t> vidxA;
		vector<size_t> eqs;

	public:
		void init(	size_t _q,
					size_t _N,
					const vector<Real> &_Jk0,
					const vector<Real> &_Jk,
					const vector<char> _invq,
					const vector<size_t> _vidxA)
		{
			order.clear();
			vidx.resize(_N);
			q = _q;
			Jk0 = _Jk0;
			Jk = _Jk;
			invq = _invq;
			vidxA = _vidxA;
		}

		void makeSystem(	const BoostGraph &g,
							const map<BoostEdge, bool> &inB,
							const map<BoostEdge, size_t> &eids,
							map<BoostEdge, bool> &visited)
		{
			// create equations
			typedef graph_traits<BoostGraph>::vertex_iterator vertex_iter;
			std::pair<vertex_iter, vertex_iter> vi = vertices(g);
			graph_traits<BoostGraph>::out_edge_iterator oei, oei_end;
			for ( tie(oei, oei_end) = out_edges(*vi.first, g); oei != oei_end; oei++ ) {
				if ( inB.at(*oei) )
					dfs(*oei, *vi.first, g, inB, eids, visited);
			}

			// rearrange A part
			vidx2 = vidx;
			for (size_t i=0; i<vidxA.size(); i++) {
				for (size_t j=0; j<order.size(); j++) {
					for (size_t k=0; k<vidx[order[j]].size(); k++) {
						if (vidx[order[j]][k] == vidxA[i])
							vidx2[order[j]][k] = i;
						else if (vidx[order[j]][k] == vidxA[i]+vidx.size()) {
							vidx2[order[j]][k] = i+vidx.size();
						}
					}
				}
			}

			// rearrange B part
			size_t newi = vidxA.size();
			for (size_t i=0; i<order.size(); i++) {
				for (size_t j=i+1; j<order.size(); j++) {
					for (size_t k=0; k<vidx[order[j]].size(); k++) {
						if (vidx[order[j]][k] == order[i])
							vidx2[order[j]][k] = newi;
						else if (vidx[order[j]][k] == order[i]+vidx.size())
							vidx2[order[j]][k] = newi+vidx.size();
					}
				}
				newi++;
			}

			// transform into a fixed vector
			for (size_t i=0; i<order.size(); i++) {
				for (size_t j=0; j<vidx2[order[i]].size(); j++)
					eqs.push_back(vidx2[order[i]][j]);
				eqs.push_back(100);
			}

			vs = vidxA.size();
			vss = vidx.size();
			eqss = eqs.size();
		}
	
		// Creates the solution of the linear system of equations
		// Traverses the graph in DFS and proceeds to update the system according to the
		// spanning tree edges contained in inB
		void dfs(	const BoostEdge &ei,
					const BoostVertex &u,
					const BoostGraph &g,
					const map<BoostEdge, bool> &inB,
					const map<BoostEdge, size_t> &eids,
					map<BoostEdge, bool> &visited)
		{
			// for each output edge 
			graph_traits<BoostGraph>::out_edge_iterator oei, oei_end;
//cout << "entering with E= " << ei << " u= " << u << " source is " << source(ei, g) << endl; 	
			for ( tie(oei, oei_end) = out_edges(target(ei, g), g); oei != oei_end; oei++ ) {
//if (target(*oei,g) == u ) 
//	cout << "edge " << *oei << " is origin " << endl;
//else
//	cout << "edge " << *oei << endl;
				if ( target(*oei, g) != u ) {
//cout << " id is " << eids.at(*oei) << " and inB[" << *oei << "] = " << inB.at(*oei) << endl;
					// if the edge is in the spanning tree, proceed recursively
					if ( inB.at(*oei) ) 
						dfs(*oei, source(*oei, g), g, inB, eids, visited);
//cout << "back from " << *oei << endl;
					
					// check if the edge has been visited
					if (!visited[*oei] || inB.at(*oei)) {

						// not visited or in the spanning tree, use the negative
						vidx[eids.at(ei)].push_back( vidx.size() + eids.at(*oei) );
						visited[*oei] = true;
					}
					else {

						// edge visited, use the value as it is
						vidx[eids.at(ei)].push_back( eids.at(*oei) );
					}
				}
			}
//cout << "finished " << ei << endl;
			// add the variable in the corresponding elimination ordering
			order.push_back(eids.at(ei));
		}
		
		Real solve( char *sol ) {
			Real prod_gamma = 1.;	// total product of potentials evaluated at corresponding value
//	cout << endl;
			// for each unknown (variable in region B)
			for (size_t i=0; i<order.size(); i++) {

				// compute the value in the sample
				size_t cur = order[i];	// ith variable in elimination ordering
				size_t sum = 0;			// sum of the values of dependent variables
//	cout << cur << "\t";
				for (size_t j=0; j<vidx[cur].size(); j++) { 
//	cout << sol[vidx[cur][j]]<< " ";
					sum += sol[vidx[cur][j]];
				}
				sol[cur] = invq[sum%q];
				sol[vidx.size() + cur] = invq[ sol[cur] ]; 
				prod_gamma *= ( sol[cur] ) ? Jk[cur]: Jk0[cur];
//	if (sol[cur]) cout << Jk[cur] << endl;
//	else cout << Jk0[cur]  << endl;
			}
			return prod_gamma;
		}

		Real solve3( char *sol ) const {
			Real prod_gamma = 1.;	// total product of potentials evaluated at corresponding value
			// for each unknown (variable in region B)
			size_t sum = 0, v;
			for (size_t i=0, var=0; i<eqss; i++) {
				v = eqs[i];
				if (v==100) {
					char a = invq[sum%q];
					sol[vs+var] = a;
					sol[vs+var+vss] = invq[a];
					prod_gamma *= (a) ? Jk[var]: Jk0[var];
					sum = 0;
					var++;
				} else {
					sum += sol[v];
				}
			}
			return prod_gamma;
		}


		Real solve_log( vector<size_t> &sol ) {
			Real log_prod_gamma = 0;	// total product of potentials evaluated at corresponding value
			// for each unknown (variable in region B)
			for (size_t i=0; i<vidx2.size(); i++) {

				// compute the value in the sample
				size_t sum = 0;
				for (size_t j=0; j<vidx2[i].size(); j++) 
					sum += sol[vidx2[i][j]];
		
				sol[vidxA.size()+i] = invq[sum%q];
				sol[vidxA.size()+i+vidx.size()] = invq[sol[vidxA.size()+i]]; 
				log_prod_gamma += ( sol[vidxA.size()+i] ) ? log(Jk[i]): log(Jk0[i]);
			}
			return exp(log_prod_gamma);
		}


        friend std::ostream& operator<< (std::ostream& os, const LinSysMod &sys) {
			os << "Linear system of equations" << endl;
			os << "\t" << sys.order.size() << " equations" << endl;
			for (size_t i=0; i<sys.order.size(); i++) {
				size_t cur = sys.order[i];
				os << "Eq (" << i << ") var " << cur << "\t";
				for (size_t j=0; j<sys.vidx[cur].size(); j++) {
					if (sys.vidx[cur][j] >= sys.vidx.size())
						os << sys.vidx[cur][j] << "(" << sys.vidx[cur][j] - sys.vidx.size() << ") ";
					else
						os << sys.vidx[cur][j] << " ";
				}
				os << endl;
			}
			os << endl;

			os << "Equivalent Linear system of equations" << endl;
			os << "\t" << sys.vidx2.size() << " equations" << endl;
			for (size_t i=0; i<sys.vidx2.size(); i++) {
				os << "Eq (" << i << ") " << "\t";
				for (size_t j=0; j<sys.vidx2[i].size(); j++) {
					if (sys.vidx2[i][j] >= sys.vidx2.size())
						os << sys.vidx2[i][j] << "(" << sys.vidx2[i][j] - sys.vidx2.size() << ") ";
					else
						os << sys.vidx2[i][j] << " ";
				}
				os << endl;
			}
			os << endl;

			return os;
		}
};

/// Approximate inference algorithm "Dual Monte Carlo"
/** Explain algorithm 
 *  here 
 */
class DUALMC : public DAIAlgFG {

    protected:
        /// Number of iterations needed
        size_t _iters;

		/// Random seed
		const gsl_rng_type *_T;
		gsl_rng *_r;

		/// Linear System
		LinSysMod _sys;

		// indices of edges in region A
		vector<size_t> _vidxA;

		// couplings in region A
		vector<Real> _vJA;

		// this is log(Z_qd) = |BA|log(q) + sum_k J_k (Eq 17)
		Real _logZq;

		// this is log(Z_ud) = |BA|log(q)
		Real _logZud;
	
		// table of inverse values
		vector<char> _invq;

		// size of state space for one variable
		size_t _q;


		Real _logZ;
	public:
        /// Parameters for DUALMC
        struct Properties {

			// method: 0 Importance sampling, 1 uniform sampling
			size_t method;
	
            /// Total number of samples
            size_t numsamples;

			/// Scaling factor of partition function
			Real scaling;

			/// Random Seed
			size_t seed;

			/// Initial iterations
			size_t burn;

            /// Verbosity (amount of output sent to stderr)
            size_t verbose;

            /// Maximum time (in seconds)
            double maxtime;

            /// Damping constant (0.0 means no damping, 1.0 is maximum damping)
            Real damping;

             /// prefix of the output filename
            string fout;
       } props;

    public:
    /// \name Constructors/destructors
    //@{
        /// Default constructor
        DUALMC() : DAIAlgFG(), _iters(0U), props() {}

        /// Construct from FactorGraph \a fg and PropertySet \a opts
        /** \param fg Factor graph.
         *  \param opts Parameters @see Properties
         */
        DUALMC( const FactorGraph & fg, const PropertySet &opts ) : DAIAlgFG(fg), _iters(0U), props() {
            setProperties( opts );
            construct();
        }

        /// Copy constructor
        DUALMC( const DUALMC &x ) : DAIAlgFG(x), _iters(0U), props(x.props) {}

        /// Assignment operator
        DUALMC& operator=( const DUALMC &x ) {
            if( this != &x ) {
                DAIAlgFG::operator=( x );
				_iters = x._iters;
                props = x.props;
            }
            return *this;
        }
    //@}

    /// \name General InfAlg interface
    //@{
        virtual DUALMC* clone() const { return new DUALMC(*this); }
        virtual DUALMC* construct( const FactorGraph &fg, const PropertySet &opts ) const { return new DUALMC( fg, opts ); }
        virtual std::string name() const { return "DUALMC"; }
        virtual Factor belief( const Var &v ) const { return beliefV( findVar( v ) ); }
        virtual Factor belief( const VarSet &vs ) const { return Factor(); }
        virtual Factor beliefV( size_t i ) const { return Factor(); }
        virtual Factor beliefF( size_t I ) const { return Factor(); }
        virtual std::vector<Factor> beliefs() const { vector<Factor> vf; return vf; }
        virtual Real logZ() const;
        std::vector<size_t> findMaximum() const { return dai::findMaximum( *this ); }
        virtual void init( const VarSet &ns );
        virtual void init();
        virtual Real run();
        virtual size_t Iterations() const { return _iters; }
        virtual void setProperties( const PropertySet &opts );
        virtual PropertySet getProperties() const;
        virtual std::string printProperties() const;
    //@}

   protected:
        /// Calculates unnormalized belief of variable \a i
        /// Calculates unnormalized belief of factor \a I
        virtual void calcBeliefF( size_t I, Prob &p ) const { std::cout << I << std::endl << p << std::endl;}
        /// Helper function for constructors
        virtual void construct() {};
};


} // end of namespace dai


#endif


#endif
