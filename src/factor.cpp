/*  This file is part of libDAI - http://www.libdai.org/
 *
 *  Copyright (c) 2006-2011, The libDAI authors. All rights reserved.
 *
 *  Use of this source code is governed by a BSD-style license that can be found in the LICENSE file.
 */


#include <dai/factor.h>


namespace dai {


using namespace std;


Factor createFactorIsing( const Var &n, Real h ) {
    DAI_ASSERT( n.states() == 2 );
    Real buf[2];
    buf[0] = std::exp(-h);
    buf[1] = std::exp(h);
    return Factor(n, &buf[0]);
}


Factor createFactorIsing( const Var &n1, const Var &n2, Real J ) {
    DAI_ASSERT( n1.states() == 2 );
    DAI_ASSERT( n2.states() == 2 );
    DAI_ASSERT( n1 != n2 );
    Real buf[4];
    buf[0] = (buf[3] = std::exp(J));
    buf[1] = (buf[2] = std::exp(-J));
    return Factor( VarSet(n1, n2), &buf[0] );
}


Factor createFactorExpGauss( const VarSet &ns, Real beta ) {
    Factor fac( ns );
    for( size_t t = 0; t < fac.nrStates(); t++ )
        fac.set( t, std::exp(rnd_stdnormal() * beta) );
    return fac;
}


Factor createFactorPotts( const Var &n1, const Var &n2, Real J ) {
    Factor fac( VarSet( n1, n2 ), 1.0 );
    DAI_ASSERT( n1.states() == n2.states() );
    for( size_t s = 0; s < n1.states(); s++ )
        fac.set( s * (n1.states() + 1), std::exp(J) );
    return fac;
}

Factor createFactorPottsDual( const Var &n1, const Var &n2, Real J ) {
    Factor fac( VarSet( n1, n2 ), 1.0 );
    DAI_ASSERT( n1.states() == n2.states() );
	size_t s=0; 
	Real val;
    for( size_t s1 = 0; s1 < n1.states(); s1++ ) {
    	for( size_t s2 = 0; s2 < n2.states(); s2++ ) {
			if (!s1 && !s2) val = std::exp(J) + n1.states()-1;
			else if ((s1+s2) == n1.states()) val = std::exp(J)-1;
			else val = 0;
//			cout << "(" << s << ") "<< s1 << " " << s2 << " = " << val << endl;
			fac.set(s,val);
			s++;
		}
	}
    return fac;
}


Factor createFactorPotts( const Var &v, Real H ) {
    Factor fac( v, 1.0 );
    fac.set( 0, std::exp(H) );
    return fac;
}

Factor createFactorDelta( const Var &v, size_t state ) {
    Factor fac( v, 0.0 );
    DAI_ASSERT( state < v.states() );
    fac.set( state, 1.0 );
    return fac;
}


Factor createFactorDelta( const VarSet& vs, size_t state ) {
    Factor fac( vs, 0.0 );
    DAI_ASSERT( (BigInt)state < vs.nrStates() );
    fac.set( state, 1.0 );
    return fac;
}

Factor createFactorIndicator( const VarSet& vs ) {
	size_t k = vs.begin()->states();
  	for( VarSet::const_iterator n = vs.begin(); n != vs.end(); n++ )
		DAI_ASSERT( n->states() == k );

    Factor fac( vs, 0.0 );
    for( size_t i = 0; i < k; i++ ) {
		Real idx = 0;
   		for( size_t j = 0; j < vs.size(); j++ ) 
			idx += i*pow(k,j);
		fac.set( idx, 1. );
	}
	return fac;
}



} // end of namespace dai
