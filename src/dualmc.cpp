/*  This file is part of libDAI - http://www.libdai.org/
 *
 *  Copyright (c) 2006-2011, The libDAI authors. All rights reserved.
 *
 *  Use of this source code is governed by a BSD-style license that can be found in the LICENSE file.
 */
 

#include <dai/dai_config.h>
#ifdef DAI_WITH_DUALMC

#include <iomanip>
#include <fstream>

#include <iostream>
#include <sstream>
#include <set>
#include <algorithm>
#include <dai/dualmc.h>
#include <dai/util.h>
#include <dai/properties.h>

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/kruskal_min_spanning_tree.hpp>

#include <gsl/gsl_randist.h>

namespace dai {

using namespace std;
using namespace boost;

#define DAI_DUALMC_FAST 1

/// \todo Make DAI_DUALMC_FAST a compile-time choice, as it is a memory/speed tradeoff

void DUALMC::setProperties( const PropertySet &opts ) {
    DAI_ASSERT( opts.hasKey("numsamples") );
    DAI_ASSERT( opts.hasKey("scaling") );

    props.numsamples = opts.getStringAs<size_t>("numsamples");
    props.scaling = opts.getStringAs<Real>("scaling");

    if( opts.hasKey("method") )
        props.method = opts.getStringAs<size_t>("method");
    else 
		props.method = 0;
    if( opts.hasKey("seed") )
        props.seed = opts.getStringAs<size_t>("seed");
    else {
		time_t tim; props.seed = time(&tim);
	}
    if( opts.hasKey("burn") )
        props.burn = opts.getStringAs<size_t>("burn");
    else
        props.burn = 0;
    if( opts.hasKey("verbose") )
        props.verbose = opts.getStringAs<size_t>("verbose");
    else
        props.verbose = 0;
	if( opts.hasKey("maxtime") )
        props.maxtime = opts.getStringAs<Real>("maxtime");
    else
        props.maxtime = INFINITY;
    if( opts.hasKey("damping") )
        props.damping = opts.getStringAs<Real>("damping");
    else
        props.damping = 0.0;
    if( opts.hasKey("fout") )
        props.fout = opts.getStringAs<string>("fout");
    else
        props.fout= string("out_dual_libdai");
}


PropertySet DUALMC::getProperties() const {
    PropertySet opts;
    opts.set( "method", props.method );
    opts.set( "numsamples", props.numsamples );
    opts.set( "scaling", props.scaling );
    opts.set( "seed", props.seed );
    opts.set( "burn", props.burn );
    opts.set( "verbose", props.verbose );
    opts.set( "maxtime", props.maxtime );
    opts.set( "damping", props.damping );
    opts.set( "fout", props.fout );
    return opts;
}


string DUALMC::printProperties() const {
    stringstream s( stringstream::out );
    s << "[";
    s << "method=" << props.method << ",";
    s << "numsamples=" << props.numsamples << ",";
    s << "scaling=" << props.scaling << ",";
    s << "seed=" << props.seed << ",";
    s << "burn=" << props.burn << ",";
    s << "verbose=" << props.verbose << ",";
    s << "maxtime=" << props.maxtime << ",";
    s << "damping=" << props.damping << ",";
    s << "fout=" << props.fout << ",";
    return s.str();
}


void DUALMC::init( const VarSet &ns ) {
	cout << "in DUALMC::init( const VarSet &ns )" << ns << endl;
}

// DUALMC::init assumes POTTS pairwaise interactions
void DUALMC::init() {

	E edge_array[ nrFactors() ];
	Real weights[ nrFactors() ];
	size_t newvar = nrVars();
	size_t num_edges = sizeof(edge_array) / sizeof(E);
	graph_traits<BoostGraph>::edge_iterator ei, ei_end;
	graph_traits<BoostGraph>::out_edge_iterator oei, oei_end;

	// create boostgraph where Node -> variable and Edge -> factor with weight J
	_q = 0;
	for( size_t I = 0; I < nrFactors(); I++ ) {
		VarSet varset;
		
		// for each variable of the factor
        VarSet::const_iterator i = factor(I).vars().begin();
		if (!_q) 
			_q = i->states();
		
        VarSet::const_iterator j = i+1;
		weights[I] = -factor(I).weight();
		if (j== factor(I).vars().end()) {
			edge_array[I] = E( i->label(), newvar );
			newvar++;
		} else {
			edge_array[I] = E( i->label(), j->label() );
		}
	}
	BoostGraph g( edge_array, edge_array + num_edges, weights, newvar );

	// precompute table of inverse values
	_invq.resize(_q,0);
	for (size_t i=1; i<_q; i++)
		_invq[i] = _q-i;
    if( props.verbose >= 2)
		cout << "table invq = " << _invq << endl;

	// initialize properties: partition, values, and weights
	map<BoostEdge, size_t> eids;	// edge indices
	map<BoostEdge, bool> inB;		// true if edge belongs to region B
	map<BoostEdge, bool> visited;	// visited true if edge already used
	size_t id = 0;
    if( props.verbose >= 2 )
		cout << "Edges" << endl;
	for (tie(ei,ei_end) = edges(g); ei != ei_end; ei++, id++) {
    	if( props.verbose >= 2 )
			cout << *ei << "\t" << id << endl;
		eids[*ei] = id;
		inB[*ei] = false;
		visited[*ei] = false;
	}

	// Compute minimum spanning tree (region B)
	vector<BoostEdge> setB;
	kruskal_minimum_spanning_tree(g, back_inserter(setB));

	// set edges in setB to true in the map inB
    if( props.verbose >= 2 )
		cout << "Edges MST (region B):" << endl;
	for (std::vector < BoostEdge >::iterator ei = setB.begin(); ei != setB.end(); ++ei) {
		inB[*ei] = true;				// set the edge to region B
	    if( props.verbose >= 2 )
			cout << eids[*ei] << " " << *ei << endl;
	}
    if( props.verbose >= 2 )
		cout << endl << "Total " << setB.size() << " edges" << endl;
	
	// precompute terms for the importance sampling 
	property_map<BoostGraph, edge_weight_t>::type weight_map = get(edge_weight, g);
	vector<Real> Jk, Jk0;							// weights in region B
	size_t sizeA = nrFactors() - setB.size();
	_logZq = (Real)sizeA*log(_q);		// this is log(Z_qd) = |BA|log(q) + sum_k J_k (Eq 17)
	_logZud = _logZq;					// this is actually for uniform sampling
	for (tie(ei,ei_end) = edges(g); ei != ei_end; ei++, id++) {
		if (!inB[*ei]) {

			// in region A
			_vidxA.push_back(eids[*ei]);
			_vJA.push_back(-weight_map[*ei]);
			_logZq += log(-weight_map[*ei]);
		}

		// precomputed terms (only those for region B will be used)
		// needed all for efficiency indexing the linear system
		Jk0.push_back( -weight_map[*ei] + _q - 1 );
		Jk.push_back( -weight_map[*ei] - 1 );
	}
    if( props.verbose >= 2 )
		cout << "region A (size " << sizeA << ")" << props.scaling << " =? " << ((Real)sizeA-1.)*log(_q) << endl;
	//	cout << "region A (size " << sizeA << ") : log Zq= " << _logZq << endl << _vidxA << endl << _vJA << endl;
	
	// traverse MST and create system of equations
	_sys.init(_q, nrFactors(), Jk0, Jk, _invq, _vidxA);
	_sys.makeSystem(g, inB, eids, visited);
	if( props.verbose >= 2 )
		cout << _sys << endl;
    _iters = 0;

	// Set scaling factor is
	props.scaling = ((Real)sizeA-1.)*log(_q); 

}

// DUALMC::run assumes POTTS pairwaise interactions
Real DUALMC::run() {

	/// random number generation
	gsl_rng_env_setup();
	_T = gsl_rng_default;
	_r = gsl_rng_alloc(_T);
	gsl_rng_set(_r, props.seed);
    if( props.verbose >= 1 ) {
        cerr << "Starting " << identify() << "...";
		printf ("generator type: %s\n", gsl_rng_name(_r));
		printf ("seed = %lu\n", props.seed);
	}

    double tic = toc();

	// the sample has size 2*E. positions beyond E, i.e. E+idx correspond to the modulus inverse of the
	// variable on region A indexed by idx (the rest of entries beyond E are ignored during sampling)
	char sample[2*nrFactors()];
	//vector<size_t> sample2(2*nrFactors());

    Real maxDiff = INFINITY;
	Real sumGammaB = 0.;
	Real sumGammaAB = 0.;
	//vector<Real> logZis(props.numsamples,0.);
	Real logZis=0.;

	// iterations for output
	Stats stat;
	size_t base = 1, curr = 1;
	vector<size_t> itdump;
	while (curr <= (size_t)props.numsamples) {
		itdump.push_back(curr);
		if (log10(curr+1)>base)
			base++;
		curr += pow(10,base-1);
	}
	if( props.verbose > 1 )
		cout << "dump at iterations " << itdump << endl;

	ofstream ouf( props.fout.c_str() );
	ouf << setprecision(15) << scientific;

	if( props.method == 0 ) { 

		// IMPORTANCE SAMPLING IN DUAL GRAPH
		if( props.verbose >= 1 ) cout << "IS " << endl; 
	    for( size_t pd=0 ; _iters < (props.numsamples+1) && (toc() - tic) < props.maxtime; _iters++ ) {
	
			// Sample from A
			for (size_t i=0; i<_vidxA.size(); i++) {
				sample[ _vidxA[i] ] = ( gsl_rng_uniform(_r) < (_q-1+_vJA[i])/(_q*_vJA[i]) ) ? 0 : (1 + (char)gsl_rng_uniform_int(_r, _q-1));
				sample[ nrFactors() + _vidxA[i] ] = _invq[sample[ _vidxA[i] ]];
			}
	
			// Solving the system to get B values
			Real GammaB = _sys.solve(sample);
			//Real GammaB3 = _sys.solve_log(sample2);
			if (_iters > props.burn) {
				sumGammaB += GammaB;
				logZis = _logZq - log(_iters-props.burn) + log(sumGammaB);
			    stat.push(logZis- props.scaling);
				if (_iters == itdump[pd]) {
					ouf << _iters << ", " << logZis - props.scaling<< ", ";
					ouf << stat.mean() << ", " << stat.variance() << endl;
					pd++;
				}
				if( props.verbose >= 3 )
					cout << "sumGammaB(" << _iters-props.burn << ") = " << sumGammaB << endl;
			}
	    }
	} else {
		// UNIFORM SAMPLING IN DUAL GRAPH
	    for( size_t pd=0 ; _iters < (props.numsamples+1) && (toc() - tic) < props.maxtime; _iters++ ) {
			Real GammaA = 1.;	// total product of potentials evaluated at corresponding value
			for (size_t i=0; i<_vidxA.size(); i++) {
				size_t s = (char)gsl_rng_uniform_int(_r, _q);		// between 0 and q-1 inclusive
				sample[ _vidxA[i] ] = s; 
				sample[ nrFactors() + _vidxA[i] ] = _invq[s];
				GammaA *= ( s ) ? (_vJA[i] - 1) : (_vJA[i] + _q - 1);
			}
	
			// Solving the system to get B values
			Real GammaB = _sys.solve(sample);
			
			if (_iters > props.burn) {
				sumGammaAB += GammaA*GammaB;
				logZis = _logZud - log(_iters-props.burn) + log(sumGammaAB);
				if (isinf(logZis))
					cout << props.burn << " " << _iters-props.burn << " " << log(_iters-props.burn) << sumGammaAB << " " << log(sumGammaAB) << endl;
			    stat.push(logZis- props.scaling);
				if (_iters >= itdump[pd]) {
					ouf << _iters << ", " << logZis - props.scaling<< ", ";
					ouf << stat.mean() << ", " << stat.variance() << endl;
					pd++;
				}
				if( props.verbose >= 3 )
					cout << "sumGammaAB(" << _iters-props.burn << ") = " << sumGammaAB << endl;
			}
	    }
	}
	// Final scaling of Z
	//			periodic grid     = sqrt(N)*log(q)
	//			non-periodic grid = ((sqrt(N)-1)^2 - 1)*log(q)
    if( props.verbose >= 1 ) {
		cout << _iters << " samples, log-scaling for should be (N = " << nrVars() << ")" << endl;
		cout << "\t\tperiodic     = " << ((Real)nrVars())*log(_q) << endl;
		cout << "\t\tnon periodic = " << (pow((sqrt((Real)nrVars())-1),2)-1)*log(_q) << endl;
		cout << "\t\tUsed is " << props.scaling << endl;
	}
	//_logZ = logZis[_iters-1] - props.scaling;//props.scaling; //- ((Real)nrVars())*log(_q);// / props.scaling;
	_logZ = logZis - props.scaling;//props.scaling; //- ((Real)nrVars())*log(_q);// / props.scaling;


//ouf << std::setprecision(15) << std::scientific << _logZ << endl;
//ouf << std::setprecision(15) << std::scientific << stat.variance() << endl;
ouf.close();

	gsl_rng_free(_r);
    return maxDiff;
}

Real DUALMC::logZ() const {
    return _logZ;
}


} // end of namespace dai




/*
	} else {

		// some precomputations
		cout << "new method (NOT FULLY TESTED!)" << endl;
		Real ratio[_vidxA.size()];
		for (size_t i=0; i<_vidxA.size(); i++)
			ratio[i] = (_q-1+_vJA[i])/(_q*_vJA[i]);
		size_t a;
		const size_t vs(_vidxA.size()), its(props.numsamples);

		/// MAIN LOOP 
	    for( ; _iters < its && (toc() - tic) < props.maxtime; _iters++ ) {
		
			// Sample from A
			for (size_t i=0; i<vs; i++) {
				a = ( gsl_rng_uniform(_r) < ratio[i] ) ? 0 : (1 + gsl_rng_uniform_int(_r, _q-1));
				sample[i]=a;
				sample[nrFactors()+i] = _invq[a];
			}
	
			// Solving the system to get B values
			Real GammaB = _sys.solve3(sample);
			props.burn = 0;
	
			if (_iters >= props.burn) {
				sumGammaB += GammaB;
				//logZis[_iters] = _logZq - log(_iters-props.burn) + log(sumGammaB);
				logZis = _logZq - log(_iters-props.burn) + log(sumGammaB);
			    if( props.verbose >= 3 )
					cout << "sumGammaB(" << _iters-props.burn << ") = " << sumGammaB << endl;
			}
		}
	}
*/



#endif
